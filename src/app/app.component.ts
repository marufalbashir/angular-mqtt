import { Component,OnDestroy } from '@angular/core';
import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnDestroy {
  title = 'angular-mqtt';
  private subscription: Subscription;
  public message: string;
  public senderName: string ='';
  public senderMessage: string ='';
  public sendTime: string ='';

  // get response of mqtt
  constructor(private _mqttService: MqttService) {
    this.subscription = this._mqttService.observe('Test').subscribe((message: IMqttMessage) => {
      this.message = message.payload.toString();
      
      const payLoadJson = JSON.parse(this.message);
      this.senderName = payLoadJson.name;
      this.senderMessage = payLoadJson.message;
      this.sendTime = payLoadJson.dateTime;
      console.log('Payload String ' + this.message);
      console.log('Sender Name ' + this.senderName);
    });
  }
  // send message to mqtt
  public unsafePublish(topic: string, sendMessage: any): void {
   
    let names = ['Maruf', 'Reza', 'Pranto','Kousher'];
    let RandomName =  names[Math.floor(Math.random() *4)];
    //alert(name);
    sendMessage = JSON.stringify({name: RandomName, age: 31, message: sendMessage, dateTime: new Date()});
   
    this._mqttService.unsafePublish(topic, sendMessage, {qos: 1, retain: true});
   // sendMessage = '';
  }

  public ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}